import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Time {


    public static void main(String[] args) throws ParseException {

        Scanner scanner = new Scanner(new InputStreamReader(System.in));
        String text = scanner.nextLine();


        String pattern = "dd.MM.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = simpleDateFormat.parse(text);
        System.out.println(date);

        int year = date.getYear() + 1900;

        if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
            System.out.println(year + " - это високосный год");
        } else {
            System.out.println(year + " - это НЕ високосный год");
        }


    }
}
